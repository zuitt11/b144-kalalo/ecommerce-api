const mongoose = require("mongoose")
const productSchema = new mongoose.Schema({
	name:{
		type:String,
		required:[true,"name is required"]
	},
		description:{
			type:String,
			required:[true,"description is required"]
	},
		price:{
			type:Number,
			required:[true,"price is required"]
	},
		isAvailable:{
			type:Boolean,
			default:true
	},
		createdOn:{
			type:Date,
			default:new Date()
	},
		reviews:[
			{
				review: {
					type:String,
					required:[true, "review is required"]
					}
			},
			{
				createdOn:{
					type:Date,
					default:new Date()
					}
			}
		]
})
module.exports=mongoose.model('Product', productSchema)