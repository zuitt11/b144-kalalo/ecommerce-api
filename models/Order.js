const mongoose = require("mongoose")
const orderSchema = new mongoose.Schema({
	userId:{
		type:String,
		required: [true, "userId is required"]
	},
	totalAmount:{
		type:Number,
		required:[true,"totalAmount is required"]
	},

	purchasedOn: {
		type:Date,
		default:new Date()
	},	
	price: {
		type:Number,
		required:[true,"totalAmount is required"]
	},
	productId: {
		type:String,
		required: [true, "productId is required"]
	},
	quantity: {
		type:Number,
		default:1
		},
	status: {
		type:String,
		default: "Waiting for payment"
		}
	})
module.exports=mongoose.model("Order", orderSchema)