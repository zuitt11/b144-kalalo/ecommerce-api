const User = require("../models/User")
const Product = require("../models/Product")
const Order = require("../models/Order")
const bcrypt = require('bcrypt');
const auth = require("../auth")



// Retrieve Active Products
module.exports.getActiveProducts=()=>{
		return Product.find({isAvailable:true}).then(result=>{
			return result
	})
}


// Retrieve All Products
module.exports.getAllProducts=()=>{
        return Product.find().then(result=>{
            return result
    })
}


//Retrieve a Product
module.exports.getProduct=(reqParams)=>{
		return Product.findById(reqParams.productId).then(result=>{
            console.log("Result: ", reqParams)
			return result
	})
}


// Create a Product
module.exports.addProduct = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin==false) {
            return "You are not an admin"
        } else {
            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newProduct.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Product creation successful"
                }
            })
        }
        
    });    
}

// Update a Product
module.exports.updateProduct = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin==false) {
            return "You are not an admin"
        } else {
                let updatedProduct = {
                    name: reqBody.name,
                    description: reqBody.description,
                    price: reqBody.price
                } 

                    return Product.findByIdAndUpdate(reqBody.productId, updatedProduct).then((result, error)=>{
                        if (error) {
                            return false
                        }else{
                            return true
                        }
                })
            }
        })
}


// archiving a product
module.exports.archiveProduct=(reqParams, userData)=>{
    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin==false) {
            return "You are not an admin"
        } else {
        let archived = {
            isAvailable: false
        } 
            return Product.findByIdAndUpdate(reqParams.productId, archived).then((result, error)=>{
                if (error) {
                    return false
                }else{
                    return true
                }
        })
    }
})
}

// unarchiving a product
module.exports.unarchiveProduct=(reqParams, userData)=>{
    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin==false) {
            return "You are not an admin"
        } else {
        let archived = {
            isAvailable: true
        } 
            return Product.findByIdAndUpdate(reqParams.productId, archived).then((result, error)=>{
                if (error) {
                    return false
                }else{
                    return true
                }
        })
    }
})
}
