const User = require("../models/User")
const Product = require("../models/Product")
const Order = require("../models/Order")
const bcrypt = require('bcrypt');
const auth = require("../auth")

module.exports.checkEmailExists = (reqBody) =>{
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0) {
			return true;
		} else{
			//no duplicate email found
			return false;
		}
	})
}

// Get all users (admin only)
module.exports.getUsers=(userData)=>{
	return User.findById(userData.userId).then(result => {
    if (userData.isAdmin==false) {
        return "You are not an admin"
     }else{
	return User.find().then(result=>{
		return result
		})
    }
	})
}


// User Registration
module.exports.registerUser=(reqBody)=>{
		return User.find({email:reqBody.email}).then(result =>{
			if (result.length>0) {
				return 'The email is already in use';
			}else{
				let newUser = new User({
					userName: reqBody.userName,
					email: reqBody.email,
					password: bcrypt.hashSync(reqBody.password, 10)
				})
				return newUser.save().then((user, error) =>{
					if(error){
						return false;
					}else{
						return true;
					}
				})
			}
		})
}

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;

	});
}

// UserAuthentication
module.exports.loginUser=(reqBody)=>{
	return User.findOne( {email:reqBody.email} ).then(result =>{
		if (result==null){
			return 'Your email does not exist';
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { accessToken: auth.createAccessToken(result.toObject()) }
			}
			else{
				return false;
				}
		}
	})
}

// Set a user as Admin
module.exports.setAsAdmin = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin==false) {
            return "You are not an admin"
        } else {
            let updatedStatus = {
            		isAdmin: reqBody.isAdmin
            	} 
            		return User.findByIdAndUpdate(reqBody.userId, updatedStatus).then((result, error)=>{
            			if (error) {
            				return false
            			}else{
            				if (result!==null) {
            					return true
            				}else{
            					return 'This user does not exist'
            				}
            			}
            	})
            }
    });    
}

// Adding a Product to a User's Wishlist
module.exports.addToWishlist=async(data, userData)=>{
	let updateWishlist = await User.findById(userData.userId).then(user => {
		user.wishlist.push({productId:data.productId, name:data.name})
		return user.save().then((user, err)=>{
			if (err) {
				return false
			}else{
				return true
			}
		})
	})
	if (updateWishlist){
		return true
	}else{
			return false
	}
}


// retrieve a user's wishlist
module.exports.getWishlist=(reqParams, userData)=>{
		return User.findById(reqParams.userId).then(result=>{
			if (userData.id !== reqParams.id) {
				return 'You cannot access this page'
			}else{
				return result.wishlist
			}
	})
}

// adding a product to user's order array
module.exports.createOrder=async(data, userData)=>{
	let updateOrder = await User.findById(userData.userId).then(user => {
		if (userData.userId!==null) {
			return Product.findById(data.productId).then(result =>{
				if (result!==null) {
		           user.orders.push({
		               totalAmount: `${data.quantity * data.price}`,
		               productId: data.productId,
		               price: data.price,
		               quantity: data.quantity
		           })
				return user.save().then((user, err)=>{
					if (err) {
						return false
				}else{
					return true
					}
					})
				}else{
						return 'Product not found'
					}
				})
		}else{
			return `You need to login to order`
		}
	})
	if (updateOrder){
		return true
	}else{
			return false
	}
	};    


// retrieve a user's orders
module.exports.getOrders=(reqParams, userData)=>{
		return User.findById(reqParams.userId).then(result=>{
			if (userData.userId!==reqParams.userId) {
				return 'You cannot access this page'
			}else{
				return result.orders
			}
	})
}