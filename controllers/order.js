const User = require("../models/User")
const Product = require("../models/Product")
const Order = require("../models/Order")
const bcrypt = require('bcrypt');
const auth = require("../auth")



// Retrieve All Orders
module.exports.getAllOrders=(userData)=>{
	return User.findById(userData.userId).then(result => {
	    if (userData.isAdmin==false) {
	    	return `You are not an admin`
	    }else{
			return Order.find().then(result=>{
				return result
			})
	    }
})	
}


// Create an order
module.exports.createOrder=(reqBody, userData)=>{
	return User.findById(userData.userId).then(result => {
		console.log("userData: ", userData.userId)
		console.log("reqBody: ", reqBody.userId)
		if (userData.userId!==null) {
			return Product.findById(reqBody.productId).then(result =>{
				if (result!==null) {
		            let newOrder = new Order({
		               userId: userData.userId,
		               productId: reqBody.productId,
		               price: result.price,
		               totalAmount: `${reqBody.quantity * result.price}`,
		               quantity: reqBody.quantity
		            })
		            console.log("Quantity: ", reqBody.quantity)
		            console.log("Price: ", result.price)
		            return newOrder.save().then((order, error) => {
		                if(error) {
		                    return false
		                } else {
		                    return "Order creation successful"
		                }
		            })
				}else{
					return 'Product not found'
				}
			})
		}else{
			return `You need to login to order`
		}
	})
};    


// Retrieve an Order
module.exports.getUserOrder=(reqBody, userData)=>{
	return User.findById(userData.userId).then(result => {
		if (userData.userId!==reqBody.userId) {
			return 'You cannot access this page'
		}else{
			return Order.findById(reqBody.orderId).then(result=>{
			if (result!==null) {
				return result
			}else{
				return 'This order does not exist'
			}
		})
		}
	})
}

// Cancel an Order
module.exports.cancelOrder=(reqParams, userData)=>{
    return User.findById(userData.userId).then(result => {
        if (userData.userId==null) {
            return "You must login first"
        } else {
        	return Order.findById(reqParams.orderId).then(result=>{
        		if (reqParams.orderId==null) {
        			return "Order id does not exist"
        		}else{
        			if (result.status=="cancelled") {
        				return "This order has been cancelled already"
        			}else{
				        let cancelledOrder = {
				            status: "cancelled"
				        } 
				            return Order.findByIdAndUpdate(reqParams.orderId, cancelledOrder).then((result, error)=>{
				                if (error) {
				                    return false
				                }else{
				                    return true
				                }
				        })
        			}
        		}
			})
		    }
    })
}


// Update the Status of an  Order
module.exports.updateStatus = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin==false) {
            return "You are not an admin"
        } else {
                let updatedStatus = {
                    status: reqBody.status
                }
                    return Order.findByIdAndUpdate(reqBody.orderId, updatedStatus).then((result, error)=>{
                        if (error) {
                            return false
                        }else{
                            return true
                        }
                })
            }
        })
}