const express = require("express")
const mongoose = require ("mongoose")
const cors = require("cors")
const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")
const orderRoutes = require("./routes/order")
const reviewRoutes = require("./routes/review")
const app = express()
const port = 4000



app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use("/api/users", userRoutes)
app.use("/api/products", productRoutes)
app.use("/api/orders", orderRoutes)
app.use("/api/reviews", reviewRoutes)


mongoose.connect("mongodb+srv://marcokalalo:admin@wdc028-course-booking.vyczd.mongodb.net/batch144_CAPSTONE_02?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology:true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open",()=> console.log(`Now connected to MongoDB Atlas`))





app.listen(process.env.PORT || port, ()=> console.log(`API now online at port ${process.env.PORT || port}`));